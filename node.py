from socket import AF_INET, SOCK_STREAM, socket, SHUT_RDWR
from concurrent.futures import ThreadPoolExecutor
from collections import defaultdict
from math import ceil

class ConsensoPaxos:
    def __init__(self, port=1000, pnumber=None, proposers=[('127.0.0.1', 1337),('127.0.0.1', 1336)], acceptors=[('127.0.0.1', 1338),('127.0.0.1', 1339)], learners=[('127.0.0.1', 1340),], proposer=False, acceptor=False, learner=False):
        print("Starting...")
        global proposal_number
        global proposal
        proposal = ''
        proposal_number = pnumber
        self._proposers = proposers
        self._acceptors = acceptors
        self._learners = learners
        self._port = port
        threads = sum([proposer, acceptor, learner])
        pool = ThreadPoolExecutor(threads)
        pool.submit(self.__proposer) if proposer else None
        pool.submit(self.__acceptor) if acceptor else None
        pool.submit(self.__learner) if learner else None
        ### Listen
        while True:
            pass


    def __proposer(self):
        global proposal_number
        resp0, resp1 = -1, -1
        sock = socket(AF_INET, SOCK_STREAM)
        sock.bind(('127.0.0.1', self._port))
        sock.listen(15)
        print("Listening...")
        try:
            while True:
                c_sock, c_addr = sock.accept()
                c_sock.settimeout(10)
                print("Connection from {}".format(c_addr))
                data = c_sock.recv(2048).decode('utf-8')
                data_split = data.split(',')
                tuple_ = '{},{},'.format(self.proposal_number(), data)
                value = int(data)
                accepted_n = 0

                for accept in self._acceptors:
                    resp = self.prepare_request(tuple_, accept)
                    if resp:
                        accepted_n += 1
                        resp0, resp1 = max(int(resp[0]), resp0), max(int(resp[1]), resp1)
                        print("Resp: ",resp0, resp1)
                
                if accepted_n == len(self._proposers):
                    for accept in self._acceptors:
                        try:
                            tuple2_ = '{},{},'.format(resp0, resp1, "accept_request")
                            self.accept_request(tuple_, accept)
                        except Exception as err:
                            print(err)
                    

                c_sock.close()
        except Exception as e:
            print(e)
        finally:
            c_sock.close()
        
    def __acceptor(self):
        self.prepare_response()

    def __learner(self):
        sock = socket(AF_INET, SOCK_STREAM)
        sock.bind(('127.0.0.1', self._port))
        sock.listen(15)
        values = defaultdict(lambda : 0)
        print("Listening...")
        try:
            while True:
                c_sock, c_addr = sock.accept()
                c_sock.settimeout(10)
                print("Connection from {}".format(c_addr))
                data = c_sock.recv(2048).decode('utf-8')
                values[data] += 1
                if values[data] > ceil(len(self._acceptors)/2):
                    print(data)
                c_sock.close()

    
    def proposal_number(self):
        global proposal_number
        return proposal_number 

    def prepare_request(self, tuple_, accept):
        sock2 = socket(AF_INET, SOCK_STREAM)
        sock2.settimeout(2)
        response = (None, None)
        try:
            print("Tuple: ", tuple_.encode())
            sock2.connect(accept)
            sock2.sendall(tuple_.encode())
            response = sock2.recv(2048).split(b',')
            print("Response: ", response)
        except Exception as err:
            print(err)
        finally:
            sock2.close()
        return response[0], response[1]

    def prepare_response(self):
        global proposal
        proposal = [-1,-1]
        sock = socket(AF_INET, SOCK_STREAM)
        sock.bind(('127.0.0.1', self._port))
        sock.listen(8)
        print("Listening...")
        try:
            while True:
                c_sock, c_addr = sock.accept()
                c_sock.settimeout(10)
                print("Connection from {}".format(c_addr))
                data = c_sock.recv(2048).decode('utf-8').split(',')
                print("Response: ",data)
                if data[2]:
                    for learner in self._learners:
                        try:
                            self.accepted(data[1], learner)
                        except Exception as err:
                            print(err)
                else:
                    if int(data[0]) > proposal[0]:
                        print(data)
                        print(proposal)
                        proposal = [int(data[0]), max(proposal[1], int(data[1]))]
                        tuple_ = '{},{}'.format(proposal[0], proposal[1]).encode()
                        c_sock.sendall(tuple_)
                c_sock.close()
#                
        except Exception as e:
            print(e)
        finally:
            c_sock.close()

    def accept_request(self, tuple_, accept):
        sock2 = socket(AF_INET, SOCK_STREAM)
        sock2.settimeout(2)
        response = (None, None)
        try:
            print("Tuple: ", tuple_.encode())
            sock2.connect(accept)
            sock2.sendall(tuple_.encode())
        except Exception as err:
            print(err)
        finally:
            sock2.close()

    def accepted(self, data, learner):
        sock2 = socket(AF_INET, SOCK_STREAM)
        sock2.settimeout(2)
        try:
            sock2.connect(learner)
            sock2.sendall(data.encode())
        except Exception as err:
            print(err)
        finally:
            sock2.close()

if __name__ == "__main__":
    import argparse
    PARSER = argparse.ArgumentParser(description="TODO")
    PARSER.add_argument("-p", "--proposer", action="store_true", help="")
    PARSER.add_argument("-a", "--acceptor", action="store_true", help="")
    PARSER.add_argument("-l", "--learner", action="store_true", help="")
    PARSER.add_argument("--pnumber", default="0", help="")
    PARSER.add_argument("--port", default="0", help="")
    ARGS = PARSER.parse_args()
    ConsensoPaxos(port=int(ARGS.port),pnumber=int(ARGS.pnumber), proposer=ARGS.proposer, acceptor=ARGS.acceptor, learner=ARGS.learner)
